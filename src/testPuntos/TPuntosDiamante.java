package testPuntos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Guerreros;
import app.Jugador;
import fwk.Diamante;

public class TPuntosDiamante {
	Jugador jugador;
	
	@Before
	public void setUp() throws Exception {
		jugador = new Guerreros("Jugador"+1);
		jugador = new Diamante(jugador);
	}

	@Test
	public void test() {
		assertTrue(jugador.getPuntos()==12);
	}

}
