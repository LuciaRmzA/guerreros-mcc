package testPuntos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Guerreros;
import app.Jugador;
import fwk.Esmeralda;
import fwk.Onice;

public class TPuntosOnice {
	Jugador jugador;
	
	@Before
	public void setUp() throws Exception {
		jugador = new Guerreros("Jugador"+1);
		jugador = new Onice(jugador);
	}
	@Test
	public void test() {
		assertTrue(jugador.getPuntos()==7);
	}

}
