package fwk;

import app.Jugador;
import app.PiedraEnum;

public class SimpleJugadorDecoradorFactory {
	
	public static Jugador crear(Jugador jugador, PiedraEnum piedra){
		
		Jugador piedraDecorador = null;
		switch (piedra) {
		case ENUM_RUBY:
			piedraDecorador = new Ruby(jugador);
			break;
		case ENUM_ESMERALDA:
			piedraDecorador = new Esmeralda(jugador);
			break;
		case ENUM_ONICE:
			piedraDecorador = new Onice(jugador);
			break;
		case ENUM_ZAFIRO:
			piedraDecorador = new Zafiro(jugador);
			break;
		case ENUM_DIAMANTE:
			piedraDecorador = new Diamante(jugador);
			break;
		}
		
		return piedraDecorador;
	}
	
	public static Jugador crearCriptonita(Jugador jugador, Integer puntos){
		return new Criptonita(jugador, puntos);
	}
}
