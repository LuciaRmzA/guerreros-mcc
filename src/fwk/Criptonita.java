package fwk;

import app.Jugador;

public class Criptonita extends JugadorDecorator {
	private Jugador jugador;
	private Integer puntos;

	public Criptonita(Jugador jugador) {
		super(jugador.getNombre());
		super.setPosicion(jugador.getPosicion());
		this.jugador = jugador;
		this.puntos = 0;
	}
	
	public Criptonita(Jugador jugador, Integer puntos) {
		super(jugador.getNombre());
		super.setPosicion(jugador.getPosicion());
		this.jugador = jugador;
		this.puntos = puntos;
	}

	@Override
	public int getPuntos() {
		return jugador.getPuntos() - puntos;
	}

	@Override
	public String getNombre() {
		return this.jugador.getNombre();
	}
}
