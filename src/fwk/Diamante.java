package fwk;

import app.Jugador;

public class Diamante extends JugadorDecorator {
	private Jugador jugador;
	private Integer puntos;

	public Diamante(Jugador jugador) {
		super(jugador.getNombre());
		super.setPosicion(jugador.getPosicion());
		this.jugador = jugador;
		this.puntos = 8;
	}
	
	@Override
	public int getPuntos() {
		return puntos + jugador.getPuntos();
	}
	

	@Override
	public String getNombre() {
		return this.jugador.getNombre();
	}

}
