package fwk;

import app.Jugador;

public abstract class JugadorDecorator extends Jugador {
	
	public JugadorDecorator(String nombre) {
		super(nombre);
		// TODO Auto-generated constructor stub
	}
	public abstract int getPuntos();
	public abstract String getNombre();
}
