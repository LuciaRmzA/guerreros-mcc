package fwk;

import app.Jugador;

public class Onice extends JugadorDecorator{
	private Jugador jugador;
	private Integer puntos;
	
	public Onice(Jugador jugador) {
		super(jugador.getNombre());
		super.setPosicion(jugador.getPosicion());
		this.jugador = jugador;
		this.puntos = 3;
	}


	@Override
	public int getPuntos() {
		return puntos + jugador.getPuntos();
	}

	@Override
	public String getNombre() {
		return this.jugador.getNombre();
	}
}
