package fwk;

import app.Jugador;

public class Ruby extends JugadorDecorator {
	private Jugador jugador;
	private Integer puntos;

	public Ruby(Jugador jugador) {
		super(jugador.getNombre());
		super.setPosicion(jugador.getPosicion());
		this.jugador = jugador;
		this.puntos = 5;
	}

	@Override
	public int getPuntos() {
		return puntos + jugador.getPuntos();
	}

	@Override
	public String getNombre() {
		return this.jugador.getNombre();
	}

}
