package testGuerrero;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Guerreros;
import app.Jugador;
import fwk.Diamante;
import fwk.Ruby;

public class TAcumulacionPuntosGuerrero {
	
	private Jugador guerrero;
	
	@Before
	public void setUp() throws Exception {
		guerrero = new Guerreros("Jugador"+1);
	}

	@Test
	public void testDecorarConRuby() {
		guerrero = new Ruby(guerrero);
		assertTrue(guerrero.getPuntos() == 9);
	}
	
	@Test
	public void testDobleDecorado() {
		guerrero = new Ruby(guerrero);
		guerrero = new Diamante(guerrero);
		
		assertTrue(guerrero.getPuntos() == 17);
	}
}
