package testGuerrero;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Simulador;

public class TLanzaDados {
	Simulador simulador;
	@Before
	public void setUp() throws Exception {
		this.simulador = new Simulador();
	}

	@Test
	public void test() {
		assertTrue(simulador.lanzaDados()>=0 && simulador.lanzaDados()<=3);
	}
}
