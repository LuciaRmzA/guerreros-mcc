package testGuerrero;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Casilla;
import app.Tablero;

public class TCrearTablero {
	Casilla[] table;
	@Before
	public void setUp() throws Exception {
		this.table = new Tablero(20).createTablero();
	}

	@Test
	public void test() {
		//Se creo el tablero con N posiciones
		assertTrue(table.length==20);
	}

}
