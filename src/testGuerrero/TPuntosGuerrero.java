package testGuerrero;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import app.Guerreros;
import app.Jugador;

public class TPuntosGuerrero {
	Jugador jugador;

	@Before
	public void setUp() throws Exception {
		jugador = new Guerreros("Jugador"+1);
	}

	@Test
	public void testCost() {
		assertTrue(jugador.getPuntos()==4);
	}
}
