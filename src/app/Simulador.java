package app;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import fwk.SimpleJugadorDecoradorFactory;

public class Simulador {
	//Atributos 
	private Random random = new Random();
	private Casilla[] tablero;
	private ArrayList<Jugador> jugadores;
	private static String SEPARADOR = "*************************************************************";
	
	public void run (){	
		// Alta de jugadores
		jugadores = altaJugadores();

		// Creación del tablero
		tablero = obtenerTablero();
		
		Boolean continuaJuego = true;
		Integer ronda = 0;
		
		do{
			ronda++;
			imprimirEstadoActualTablero(ronda);
						
			jugadores.forEach(jugador -> {		
				if(jugador.status){
					this.siguienteTurno(jugador);
				}	
			});
			
			Integer numeroJugadoresFuera = jugadores.stream()
					.mapToInt(jugador-> !jugador.status ? 1 : 0)
					.sum();
			
			if(numeroJugadoresFuera == jugadores.size()){
				continuaJuego = false;
			}
			
		}while(continuaJuego);
		
		Jugador jugadorGanador = evaluaGanador(jugadores);
		
		System.out.println("El jugador ganador es !!!!!!  "+ jugadorGanador.getNombre());
	}

	private ArrayList<Jugador> altaJugadores(){
		ArrayList<Jugador> jugadores = new ArrayList<>();
				
		IntStream.rangeClosed(1, 3).forEach( numeroJugador -> {
			Jugador jugador = new Guerreros("Jugador"+numeroJugador);
			jugador.getPuntos();
			jugadores.add(jugador);
		});
		
		return jugadores;
	}
	
	private StatusCombate combatir(Jugador primerJugador, Jugador segundoJugador){
		System.out.println("Combaten: " + primerJugador.getNombre() + "("+primerJugador.getPuntos()+") vs " + segundoJugador.getNombre()+"("+segundoJugador.getPuntos()+")");
		StatusCombate statusCombate = StatusCombate.GANO;
		
		if (primerJugador.getPuntos() < segundoJugador.getPuntos()){
			statusCombate = StatusCombate.PERDIO;
		}else if(primerJugador.getPuntos() == segundoJugador.getPuntos()){
			statusCombate = StatusCombate.EMPATE;
		}
		
		return statusCombate;
	}
	
	public Jugador decorar(Jugador jugador, PiedraEnum piedra){
		return SimpleJugadorDecoradorFactory.crear(jugador, piedra);
	}
	
	public Jugador decorarCriptonita(Jugador jugador, Integer puntosCriptonita){
		return SimpleJugadorDecoradorFactory.crearCriptonita(jugador, puntosCriptonita);
	}
	
	private Jugador evaluaGanador(ArrayList<Jugador> jugadores){
		Jugador jugadorGanador = jugadores.get(0);

		for(int i = 1; i < jugadores.size(); i++ ){
			Jugador jugadorAComparar = jugadores.get(i);

			if(jugadorAComparar.getPuntos() > jugadorGanador.getPuntos()){
				jugadorGanador = jugadorAComparar;
			} 
		}

		return jugadorGanador;
	}
	
	private Casilla[] obtenerTablero(){
		return new Tablero(20).createTablero();
	}
	
	public Integer lanzaDados(){
		return random.nextInt(3)+1;
	} 
	
	private void siguienteTurno(Jugador jugador){	
		System.out.println("------------------------------------------");
		System.out.println("TURNO - " +jugador.getNombre().toUpperCase());
		System.out.println("------------------------------------------");
		
		Integer resultadoDado = lanzaDados();
		Integer posicionActual = jugador.getPosicion();
		System.out.println("Posicion actual: " + posicionActual);
		System.out.println("Puntos actuales : "+ jugador.getPuntos());
		System.out.println("Lanzó dado: " + resultadoDado);
		Integer siguientePosicion = posicionActual + resultadoDado;
		System.out.println("Siguiente posición: " + siguientePosicion);
		
		if(siguientePosicion >= this.tablero.length){
			jugador.setStatus(false);
		}else {
			
			Casilla siguienteCasilla = this.tablero[siguientePosicion];
			ArrayList<PiedraEnum> piedras = siguienteCasilla.getPiedras();
			if(piedras.size() > 0){
				PiedraEnum piedraEnum = piedras.get(0);
				System.out.println("Obtuvo: " + piedraEnum.getNombre());
				jugador = this.decorar(jugador, piedraEnum);
								
				System.out.println("Nuevos puntos: " + jugador.getPuntos());
				
				piedras.remove(0);
			}
			
			ArrayList<Jugador> jugadoresSiguienteCasilla = jugadoresEnPosicion(siguientePosicion);
			
			for(Jugador jugadorSiguienteCasilla : jugadoresSiguienteCasilla){
				
				// Si el jugador ganó el método combatir regresa true
				StatusCombate statusCombate = this.combatir(jugador, jugadorSiguienteCasilla);
				
				switch (statusCombate) {
					case GANO:
						Integer diferentecia =   jugador.getPuntos() - jugadorSiguienteCasilla.getPuntos();
						jugadorSiguienteCasilla = this.decorarCriptonita(jugadorSiguienteCasilla, diferentecia);
	
						if(jugadorSiguienteCasilla.getPuntos() <= 0){
							jugadorSiguienteCasilla.setStatus(false);
						}
						
						System.out.println(jugador.getNombre() + " GANO	(" + jugador.getPuntos() + " puntos)");
						System.out.println(jugadorSiguienteCasilla.getNombre() + " PERDIO	(" + jugadorSiguienteCasilla.getPuntos() + " puntos)");
	
						this.actualizaJugador(jugadorSiguienteCasilla);
						break;
					case PERDIO:
						
						Integer diferenteciaPuntos =  jugadorSiguienteCasilla.getPuntos() - jugador.getPuntos();
						jugador = this.decorarCriptonita(jugador, diferenteciaPuntos);
	
						if(jugador.getPuntos() <= 0){
							jugador.setStatus(false);
						}
						
						System.out.println(jugadorSiguienteCasilla.getNombre() + " GANO	(" + jugadorSiguienteCasilla.getPuntos() + " puntos)");
						System.out.println(jugador.getNombre() + " PERDIO	(" + jugador.getPuntos() + " puntos)");
						
						break;
					case EMPATE:
						System.out.println(jugadorSiguienteCasilla.getNombre() + "	(" + jugadorSiguienteCasilla.getPuntos() + " puntos)");
						System.out.println(jugador.getNombre() + "	(" + jugador.getPuntos() + " puntos)");			
					break;
				}
			}
		}
		
		jugador.setPosicion(siguientePosicion);
		this.actualizaJugador(jugador);
		System.out.println("------------------------------------------");
	}
	
	public ArrayList<Jugador> jugadoresEnPosicion(Integer posicion){
		 return this.jugadores.stream()
				 .filter(jugador -> jugador.getPosicion().equals(posicion))
				 .collect(Collectors.toCollection(ArrayList::new));
	}
	
	public void imprimirEstadoActualTablero(Integer ronda){
		System.out.println(SEPARADOR);
		System.out.println("ESTADO DEL TABLERO - RONDA "+ ronda);
		System.out.println(SEPARADOR);
		StringBuilder stb = new StringBuilder();
		
		for(int i = 0; i < this.tablero.length; i++){
			stb.delete(0, stb.length());
			Casilla casilla = this.tablero[i];
			ArrayList<PiedraEnum> piedras = casilla.getPiedras();
			
			System.out.print(String.format("Casilla [%d] : ", i ));
			
			if(piedras.size() > 0){
				for(PiedraEnum piedra : piedras){
					stb.append(piedra.getNombre() + ",");
				}
			}else{
				stb.append("No tiene Piedras");
			}
			
			System.out.print(stb.toString());
			System.out.println("");
		}
		
		System.out.println(SEPARADOR);
	}
	
	public void actualizaJugador(Jugador jugadorActual){
		for(int i = 0; i< this.jugadores.size(); i++){
			Jugador jugador = this.jugadores.get(i);
			
			if(jugador.equals(jugadorActual)){
				this.jugadores.set(i, jugadorActual);
			}
		}
	}
}
