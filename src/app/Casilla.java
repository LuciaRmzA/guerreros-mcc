package app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Casilla {
	//Atributos
	private int posicion;
	private ArrayList<PiedraEnum>piedras = new ArrayList<>();
	
	//Constructor
	public Casilla(int posicion, int numPiedras){
		this.posicion = posicion;
		this.setPiedras(numPiedras);
	}

	//Metodos
	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public ArrayList<PiedraEnum> getPiedras() {
		return piedras;
	}

	public void setPiedras(int numPiedras) {
		PiedraEnum [] piedrasEnums = PiedraEnum.values();
		
		List<PiedraEnum> piedrasList =Arrays.asList(piedrasEnums);
		
		List<PiedraEnum> piedrasSinCriptonita = piedrasList.stream()
		.filter(piedra-> piedra != PiedraEnum.ENUM_CRIPTONITA)
		.collect(Collectors.toList());
		
		for(int i=0; i < numPiedras; i++){
			Integer tipoPiedra = new Random().nextInt(piedrasSinCriptonita.size());
			PiedraEnum piedraEnum = piedrasSinCriptonita.get(tipoPiedra);
			piedras.add(piedraEnum);		//Existen 5 tipos de piedras
		}
		
		Collections.sort(piedras);
	}
	
	@Override
	public String toString() {
		System.out.println("CASILLA ["+this.getPosicion()+"] piedras="+piedras.size());
		for(PiedraEnum piedraEnum:piedras){
			System.out.println("Piedra= "+piedraEnum);
		}
		return "";
	}
}
