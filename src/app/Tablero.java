package app;

import java.util.Random;
import java.util.stream.IntStream;

public class Tablero {
	//Atributos
	private Casilla[] tablero;
	private Random random;
	
	//Constructor
	public Tablero(int numCasillas){
		this.tablero = new Casilla[numCasillas];
		this.random = new Random();
	}
	
	//Metodos
	public Casilla[] createTablero(){
		IntStream.rangeClosed(0, tablero.length-1).forEach(numeroCasillas -> {
			this.tablero[numeroCasillas] = new Casilla(numeroCasillas, new Random().nextInt(4));
		});
		
		return this.tablero;
	}

	public Casilla[] getTablero() {
		return tablero;
	}
}
