package app;

public enum PiedraEnum {
	ENUM_DIAMANTE("Diamante"),
	ENUM_RUBY("Ruby"),
	ENUM_ZAFIRO("Zafiro"),
	ENUM_ESMERALDA("Esmeralda"),
	ENUM_ONICE("Onice"),
	ENUM_CRIPTONITA("Criptonita");

	private String nombre;

	private PiedraEnum(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre(){
		return this.nombre;
	}
}
