package app;


public class Guerreros extends Jugador{

	public Guerreros(String nombre){
		super(nombre);
	}

	@Override
	public int getPuntos() {
		return 4;
	}
}
