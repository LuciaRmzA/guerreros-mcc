package app;

public abstract class Jugador {
	//Atributos
	protected String nombre;
	protected boolean status;
	protected Integer posicion = 0;

	
	public Jugador(String nombre){
		this.nombre = nombre;
		this.status = true;
	}

	//Metodos
	public abstract int getPuntos();

	public String getNombre() {
		return nombre;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String toString(){
		return this.nombre + " - " + this.getPuntos();
	}
	
	public boolean equals(Object o){
		
		if(!(o instanceof Jugador))
			return false;

		return ((Jugador) o).getNombre().compareTo(this.getNombre()) ==0;
	}
	
	public int hashCode(){
	    return this.getNombre().hashCode();
	}
	
	public Integer getPosicion(){
		return this.posicion;
	}
	
	public void setPosicion(Integer posicion){
		this.posicion = posicion;
	}
}

