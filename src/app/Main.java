package app;

public class Main {
	
	private Simulador simulador;
	
	public static void main(String args[]){
		Main main = new Main();
		main.run();
	}
	
	public void run(){
		
		simulador = new Simulador();
		simulador.run();
		
	}
}
